set(FREERTOS_LIB_TYPE "STATIC")

set(FREERTOS_PORT_TOOL "GCC")
set(FREERTOS_PORT "ARM_CM33_NTZ/non_secure")

set(FREERTOS_HEAP4 1)

add_definitions(-DFREERTOS_LIB)

set(FREERTOS_PORT_EXTRA_C_SRC
    FreeRTOS/Source/portable/GCC/ARM_CM33_NTZ/non_secure/portasm.c
)
