set(FREERTOS_LIB_TYPE "STATIC")

set(FREERTOS_PORT_TOOL "GCC")
set(FREERTOS_PORT "ARM_CM33/non_secure")

set(FREERTOS_HEAP4 1)

# set(FREERTOS_PLUS_TRACE 1)

add_definitions(-DFREERTOS_LIB)

set(FREERTOS_PORT_EXTRA_C_SRC
    FreeRTOS/Source/portable/GCC/ARM_CM33/secure/secure_context_port.c
    FreeRTOS/Source/portable/GCC/ARM_CM33/secure/secure_context.c
    FreeRTOS/Source/portable/GCC/ARM_CM33/secure/secure_heap.c
    FreeRTOS/Source/portable/GCC/ARM_CM33/secure/secure_init.c
)

set(FREERTOS_PORT_EXTRA_INC
    FreeRTOS/Source/portable/GCC/ARM_CM33/secure
)

set(FREERTOS_EXTRA_C_SRC 
                         )
