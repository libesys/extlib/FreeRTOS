set(FREERTOS_CONFIG_DIR ${FREERTOS_DIR}/include/FreeRTOS/win32)
set(FREERTOS_LIB_TYPE "SHARED")

set(FREERTOS_PORT_TOOL "ThirdParty/GCC")
set(FREERTOS_PORT "Posix")
add_definitions(-DFREERTOS_PORT_POSIX)

set(FREERTOS_HEAP5 1)

set(FREERTOS_PLUS_TRACE 1)

add_definitions(-DFREERTOS_LIB)

set(FREERTOS_PORT_EXTRA_C_SRC
    FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c)

set(FREERTOS_EXTRA_C_SRC src/FreeRTOS/win32/main.c
                         src/FreeRTOS/win32/Run-time-stats-utils.c)
