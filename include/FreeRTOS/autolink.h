/*!
 * \file FreeRTOS/autolink.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#if !defined(FREERTOS_EXPORTS) && defined(_MSC_VER)
#ifdef ESYS_LIB_NAME
#undef ESYS_LIB_NAME
#endif
#define ESYS_LIB_NAME freertos

#ifdef ESYS_LIB_VERSION
#undef ESYS_LIB_VERSION
#endif

#define ESYS_LIB_VERSION "0.1.0"
#include <esysmsvc/autolink.h>
#endif
